Website
=======
http://www.remotesensing.org/libtiff/

License
=======
ZLIB like license (see the file source/COPYRIGHT)

Version
=======
4.0.3

Source
======
tiff-4.0.3.tar.gz (sha256: ea1aebe282319537fb2d4d7805f478dd4e0e05c33d0928baba76a7c963684872)
